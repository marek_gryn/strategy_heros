import javax.xml.transform.dom.DOMLocator;

/**
 * Created by Marek on 17.08.2017.
 */
public abstract class AbstractSoldier {
    private Double expirience_points;
    private Double health_points=1000.0;
    private Double arrowDefPoints;
    private Double phisicalDefPoints;
    private Double spellDefPoints;

    private Double pointsAfterFight;

    public Double getExpirience_points() {
        return expirience_points;
    }

    public void setExpirience_points(Double expirience_points) {
        this.expirience_points = expirience_points;
    }

    public Double getHealth_points() {
        return health_points;
    }

    public void setHealth_points(Double health_points) {
        this.health_points = health_points;
    }

    public Double getArrowDefPoints() {
        return arrowDefPoints;
    }

    public void setArrowDefPoints(Double arrowDefPoints) {
        this.arrowDefPoints = arrowDefPoints;
    }

    public Double getPhisicalDefPoints() {
        return phisicalDefPoints;
    }

    public void setPhisicalDefPoints(Double phisicalDefPoints) {
        this.phisicalDefPoints = phisicalDefPoints;
    }

    public Double getSpellDefPoints() {
        return spellDefPoints;
    }

    public void setSpellDefPoints(Double spellDefPoints) {
        this.spellDefPoints = spellDefPoints;
    }

    public boolean fight (AbstractSoldier soldierToFight){
        pointsAfterFight = soldierToFight.getHealth_points()-(getExpirience_points()/10.0);
        return true;
    }
}
